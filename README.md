# Hybrid-Colors

## MacOS Terminal
import `hybrid.terminal`

## MacOS iTerm
import `hybrid.itermcolors`

## Terminal
Add to ~/.profile
```
if tput setaf 1 &> /dev/null; then
	tput sgr0; # reset colors
	bold=$(tput bold);
	reset=$(tput sgr0);
	white=$(tput setaf 15);
	dark_red=$(tput setaf 8);
	black=$(tput setaf 0);
	light_gray=$(tput setaf 7);
	light_red=$(tput setaf 9);
	dark_yellow=$(tput setaf 3);
	light_yellow=$(tput setaf 11);
	light_green=$(tput setaf 10);
	aqua=$(tput setaf 14);
	light_blue=$(tput setaf 12);
	light_magenta=$(tput setaf 13);
	foreground=$(tput setaf 250);
	selection=$(tput setaf 237);
	line=$(tput setaf 235);
	comment=$(tput setaf 243);
	red=$(tput setaf 167);
	orange=$(tput setaf 173);
	yellow=$(tput setaf 221);
	green=$(tput setaf 143);
	light_cyan=$(tput setaf 109);
	blue=$(tput setaf 110);
	purple=$(tput setaf 139);
	delbg=$(tput setaf 167);
fi;


REAL_NAME=$(id -P $(stat -f%Su /dev/console) | cut -d : -f 8)
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\[${bold}${light_grey}\]\$REAL_NAME [\[$light_green\]\w\[$white\]\$([[ -n \$(git branch 2> /dev/null) ]] && echo \" - \")\[$purple\]\$(parse_git_branch)\[$white\]] \$ \[$reset\]"

# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'

# Tell ls to be colourful
export CLICOLOR=1
export LSCOLORS=Exfxcxdxbxegedabagacad
```

## Vim
Copy colors/hybrid.vim to:
```
~/.vim/colors/hybrid.vim
```

Add to ~/.vimrc
```
set background=dark
colorscheme hybrid
```
